import 'dart:async';

import 'package:example_location/repository/gps/gps-respository.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  LocationData currentLocation;

  GpsRepository _gpsRepository = GpsRepository();
  String messageError;

  @override
  void initState() {
    new Timer.periodic(const Duration(minutes: 2), (timer) {
      print("En LA FUNCION PERIODIC");
      _getLocationDevice();
    });
    super.initState();
  }

  void _getLocationDevice()  async {
    try {
      LocationData res = await _gpsRepository.getLocation();
      setState(() {
        currentLocation = res;
      });
    }catch(error) {
      setState(() {
        messageError = error.toString();
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Location ${currentLocation != null ? currentLocation.latitude : 'o'} '
                  '${currentLocation != null ? currentLocation.longitude : 'o'}',
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _getLocationDevice,
        tooltip: 'Increment',
        child: Icon(Icons.pin_drop),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
