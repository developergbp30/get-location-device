

import 'package:location/location.dart';

class GpsRepository {
   Location _location = new Location();

  Future<LocationData> getLocation() async {
    try{
      return await _location.getLocation();
    } catch (error){
      if (error.code == 'PERMISSION_DENIED') {
        String message = 'Permission denied';
        return Future.error(message);
      }
    }
  }
}